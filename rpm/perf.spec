%define kernel_version  4.4
Name: perf-%{kernel_version}
Summary:    Linux kernel performance auditing tool
Version:    4.4.176
Release:    1
Group:      Kernel/Linux Kernel
License:    GPLv2
Source0:    %{name}-%{version}.tar.xz
Requires:   module-init-tools
BuildRequires:  pkgconfig(ncurses)
BuildRequires:  module-init-tools
BuildRequires:  fdupes

BuildRequires:  elfutils-devel
BuildRequires:  binutils-devel
BuildRequires:  flex
BuildRequires:  bison

%description
%{Summary}


%prep
%setup -q -n %{name}-%{version}

chmod +x linux/tools/perf/util/generate-cmdlist.sh

%build

cd linux/tools/perf
make -f Makefile.perf \
     ASCIIDOC8=1 \
     all \
     prefix=/usr \
     perfexecdir=%{_libexecdir}/%{name} \
     NO_LIBBPF=1 # not aviable on armv7hl
cd ..
cd ..


# >> build post
# << build post

%install
rm -rf %{buildroot}

cd linux/tools/perf
make -f Makefile.perf\
    install \
    prefix=/usr \
    perfexecdir=%{_libexecdir}/%{name} \
    DESTDIR=%{buildroot} \
    NO_LIBBPF=1 # not aviable on armv7hl

cd ..
cd ..
#mkdir -p %{buildroot}/%{kernel_devel_dir}
#cp -a scripts %{buildroot}/%{kernel_devel_dir}

#%fdupes  %{buildroot}/%{_prefix}/src/kernels/%{kernel_version}/

%files
%defattr(-,root,root,-)
/usr/bin/perf
/usr/libexec/%{name}/*
/etc/bash_completion.d/perf
/usr/bin/trace
/usr/lib/traceevent/plugins/*
